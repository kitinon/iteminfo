package com.hiqfood.iteminfo;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.event.InputEvent;

import com.hiqfood.navision.ro.*;

public class SearchVM {
	String searchString;
	List<Item> matchedItems;
	Item selectedItem;
	boolean includeFG = true;
	boolean includeWIP = false;
	boolean includeRM = false;
	private String errorMessage;

	@Command @NotifyChange("matchedItems")
	public void search(@ContextParam(ContextType.TRIGGER_EVENT) InputEvent event) {
		if (event.getClass() != InputEvent.class) return;
		this.searchString = ((InputEvent)event).getValue();
		search();
	}

	private void search() {
		Collection<Item> items;
		try {
			items = Item.getItems(ItemFilter.or(ItemFilter.IPG_FG, ItemFilter.IPG_WIP + ItemFilter.IPG_RM)).values();
			int length = this.searchString.trim().length();
			this.matchedItems = new ArrayList<Item>();
			if (length < 2) return;
			String[] subStrings = this.searchString.toLowerCase().split("\\s+");
			for (Item item : items) {
				String type = item.getType();
				if (!((type.equals("RM") && this.includeRM)
						|| (type.equals("WIP") && this.includeWIP) 
						|| (type.equals("FG") && this.includeFG))
				   ) continue;
				boolean found = true;
				String desc = item.getNo_() + " " + item.getDescription() + " " + item.getBrand();
				for (String s : subStrings) {
					if (desc.toLowerCase().indexOf(s) < 0) {
						found = false;
						break;
					}
				}
				if (found) this.matchedItems.add(item);
			}
			Collections.sort(this.matchedItems);
		} catch (Exception e) {
			errorMessage = e.getMessage();
		}
	}
	
	public List<Item> getItems() throws ClassNotFoundException, SQLException, IOException {
		return new ArrayList<Item>(Item.getItems(ItemFilter.or(ItemFilter.IPG_FG, ItemFilter.IPG_WIP + ItemFilter.IPG_RM)).values());
	}
	
	public String getSearchString() {
		return this.searchString;
	}
	
	public List<Item> getMatchedItems() {
		return this.matchedItems;
	}

	public boolean isIncludeFG() {
		return this.includeFG;
	}

	@NotifyChange("matchedItems")
	public void setIncludeFG(boolean includeFG) {
		this.includeFG = includeFG;
		search();
	}

	public boolean isIncludeWIP() {
		return this.includeWIP;
	}

	@NotifyChange("matchedItems")
	public void setIncludeWIP(boolean includeWIP) {
		this.includeWIP = includeWIP;
		search();
	}

	public boolean isIncludeRM() {
		return this.includeRM;
	}

	@NotifyChange("matchedItems")
	public void setIncludeRM(boolean includeRM) {
		this.includeRM = includeRM;
		search();
	}

	public Item getSelectedItem() {
		return this.selectedItem;
	}

	public void setSelectedItem(Item selectedItem) {
		this.selectedItem = selectedItem;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
	
	public Double getInventoryLevel(String itemNo_) {
		try {
			return Inventory.getTotalInventory(itemNo_);
			//return Item.getItem(itemNo_).getInventoryLevel();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String getBaseUnit(String itemNo_) {
		try {
			return Item.getItem(itemNo_).getBaseUnit();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}