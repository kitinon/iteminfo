package com.hiqfood.iteminfo;

import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.*;

@SuppressWarnings("serial")
public class ItemTreeController extends GenericForwardComposer<Div> {
	@Wire
	private Tree itemTree;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public void doAfterCompose(Div div) throws Exception {
        super.doAfterCompose(div);
		TreeModel model = new DefaultTreeModel(
				  new DefaultTreeNode(null,
				    new DefaultTreeNode[] {
				      new DefaultTreeNode("A"),
				      new DefaultTreeNode("B",
				        new DefaultTreeNode[] {
				          new DefaultTreeNode("C",
				            new DefaultTreeNode[] {
				              new DefaultTreeNode("D"),
				              new DefaultTreeNode("E")
				            }),
				          new DefaultTreeNode("F"),
				          new DefaultTreeNode("G")
				        })
				      }
				  ));
		itemTree.setModel(model);
	}
}
