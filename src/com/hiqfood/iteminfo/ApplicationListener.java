package com.hiqfood.iteminfo;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import com.hiqfood.navision.ro.DAL;

public class ApplicationListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		//Cache.setActive(false);		
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		DAL.shutdown();
	}
}
