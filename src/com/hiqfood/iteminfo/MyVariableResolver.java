package com.hiqfood.iteminfo;

import org.zkoss.xel.VariableResolver;
import org.zkoss.xel.XelException;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;

public class MyVariableResolver implements VariableResolver {

	@Override
	public Object resolveVariable(String name) throws XelException {
		if (name.equals("SearchVM")) {
			Session session = Executions.getCurrent().getSession();
			Object searchVM = session.getAttribute("SearchVM");
			if (searchVM ==null) {
				searchVM = new SearchVM();
				session.setAttribute("SearchVM", searchVM);
			}
			return searchVM;
		}
		return null;
	}
}
