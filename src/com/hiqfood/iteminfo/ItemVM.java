package com.hiqfood.iteminfo;

import java.util.Map;

import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;

import com.hiqfood.navision.ro.*;

public class ItemVM {
	Item selectedItem;
	private String errorMessage;

	@Init
	public void init() {
		String itemNo_ = Executions.getCurrent().getParameter("itemNo_");
		if (itemNo_ != null) searchByNo_(itemNo_);
	}
	
	public ItemVM() {
		super();
	}
	
	public ItemVM(String itemNo_) {
		searchByNo_(itemNo_);
	}
	
	@NotifyChange("selectedItem")
	private void searchByNo_(String itemNo_) {
		Map<String, Item> items;
		try {
			items = Item.getItems(ItemFilter.or(ItemFilter.IPG_FG, ItemFilter.IPG_WIP + ItemFilter.IPG_RM));
			this.selectedItem = items.get(itemNo_);
		} catch (Exception e) {
			errorMessage = e.getMessage();
		}
	}

	public Item getSelectedItem() {
		return this.selectedItem;
	}

	public void setSelectedItem(Item selectedItem) {
		this.selectedItem = selectedItem;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
}